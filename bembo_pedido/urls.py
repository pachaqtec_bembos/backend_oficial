from django.urls import path, include
from .views import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
# router.register(r'auth_users', authUserViewSet)
router.register(r'roles', RoleViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'estatus', StatusViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orders_detail', Order_detailViewSet)

urlpatterns = [
    path('bembo_pedido/', include(router.urls)),
]