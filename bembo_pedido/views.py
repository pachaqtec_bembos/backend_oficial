from django.shortcuts import render
from rest_framework import viewsets, filters, permissions
from .serializers import *
from .models import *


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

"""
class authUserViewSet(viewsets.ModelViewSet):
    serializer_class = authUserSerializer
    queryset = authUser.objects.all()
"""

class RoleViewSet(viewsets.ModelViewSet):
    serializer_class = RoleSerializer
    queryset = Role.objects.all()

class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = InvoiceSerializer
    queryset = Invoice.objects.all()

class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

class StatusViewSet(viewsets.ModelViewSet):
    serializer_class = StatusSerializer
    queryset = Status.objects.all()

class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

class Order_detailViewSet(viewsets.ModelViewSet):
    serializer_class = Order_detailSerializer
    queryset = Order_detail.objects.all()
