from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(User)
admin.site.register(Role)
admin.site.register(Invoice)
admin.site.register(Order)
admin.site.register(Status)
admin.site.register(Supply)
admin.site.register(Order_detail)
admin.site.register(Category)