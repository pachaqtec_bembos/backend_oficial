from django.apps import AppConfig


class BemboPedidoConfig(AppConfig):
    name = 'bembo_pedido'
