from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User as authUser

# Create your models here.

"""
class User(models.Model):
    id = models.AutoField(primary_key=True)
    document = models.CharField(max_length=8, blank=False, null=False)
    first_name = models.CharField(max_length=45, blank=False, null=False)
    last_name = models.CharField(max_length=45, blank=False, null=False)
    ##image= models.ImageField(upload_to='/',blank=True, null=False)
    user = models.OneToOneField(authUser, on_delete=models.CASCADE)
    rol = models.ForeignKey('Role', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name= 'User'
        verbose_name_plural= 'Users'
        ordering = ['last_name']

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
"""
class User(authUser):
    ##image= models.ImageField(upload_to='/',blank=True, null=False)
    rol_id = models.ForeignKey('Role', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name= 'Usuario'
        verbose_name_plural= 'Usuarios'
        ordering = ['last_name']

    def __str__(self):
        return f'{self.first_name} {self.last_name}'



class Role (models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=False, null=False)

    class Meta:
        verbose_name= 'Rol'
        verbose_name_plural= 'Roles'
        ordering = ['name']

    def __str__(self):
        return self.name



class Invoice(models.Model):
    id = models.AutoField(primary_key=True)
    cod_invoice = models.CharField(max_length=30, blank=False, null=False)
    amount = models.CharField(max_length=10, blank=False, null=False)
    date = models.DateTimeField(default=timezone.now)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Factura'
        verbose_name_plural= 'Facturas'
        ordering = ['cod_invoice']

    def __str__(self):
        return self.cod_invoice



class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, blank=False, null= False) 

    class Meta:
        verbose_name= 'Categoría'
        verbose_name_plural= 'Categorías'
        ordering = ['name']

    def __str__(self):
        return self.name



class Supply(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False, null= False) 
    price = models.FloatField(blank=False, null=False)
    ##image = models.ImageField(upload_to='/',blank=True, null=False)
    quantity = models.IntegerField(blank=False, null= False) 
    category_id = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Insumo'
        verbose_name_plural= 'Insumos'
        ordering = ['name']

    def __str__(self):
        return self.name



class Status(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, blank=False, null= False)
    
    class Meta:
        verbose_name= 'Estado'
        verbose_name_plural= 'Estados'
        ordering = ['name']

    def __str__(self):
        return self.name



class Order(models.Model):
    id = models.AutoField(primary_key=True)
    cod_order = models.CharField(max_length=30, blank=False, null=False)
    invoice_id = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    fecha = models.DateTimeField(default=timezone.now)
    status_id = models.ForeignKey(Status, on_delete=models.CASCADE)
    supply = models.ManyToManyField(Supply, through='Order_detail')
    

    class Meta:
        verbose_name= 'Orden'
        verbose_name_plural= 'Órdenes'
        ordering = ['cod_order']

    def __str__(self):
        return self.cod_order



class Order_detail(models.Model):
    id = models.AutoField(primary_key=True)
    quantity = models.IntegerField(blank=False, null= False)
    price = models.FloatField(blank=False, null=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    supply = models.ForeignKey(Supply, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Detalle de Orden'
        verbose_name_plural= 'Detalle de Órdenes'
        

    def __str__(self):
        return self.order.cod_order
